from pydoc import render_doc
from flask import Flask, render_template, render_template_string, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///prob.db"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
db.init_app(app)

def init_db():
    db.init_app(app)
    db.app = app
    db.create_all()

class prob(db.Model):
    sno = db.Column(db.Integer, primary_key=True)    
    np = db.Column(db.Integer, nullable=False)
    oih = db.Column(db.Integer, nullable=False)
    

    def __refr__(self) -> str:
        return f"{self.np} -  {self.oih}"  

@app.route("/", methods=['GET','POST'])
    
def hello_world():
    if request.method=="POST":
        np = request.form['noticePeriod']
        oih = request.form['OfferInHand']
        Prob = prob(np=np, oih=oih)
        db.session.add(Prob)
        db.session.commit()

        
        #print(oih) 
        #print("Hi")
    
    return render_template('index.html',oih=oih)    

if __name__ == "__main__":
    app.run(debug=True, port=8000)
    